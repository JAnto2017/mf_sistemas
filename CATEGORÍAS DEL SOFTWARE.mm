<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#0000ff" CREATED="1633942903917" ID="ID_1159841980" MODIFIED="1633943428979" TEXT="CATEGOR&#xcd;AS DEL SOFTWARE">
<node COLOR="#ff00ff" CREATED="1633942925978" ID="ID_229661294" MODIFIED="1633943459344" POSITION="right" TEXT="Software de dominio p&#xfa;blico">
<icon BUILTIN="forward"/>
<node CREATED="1633943012506" ID="ID_269929663" MODIFIED="1633943076818" TEXT="No est&#xe1; protegido por copyright (derecho de explotaci&#xf3;n">
<icon BUILTIN="full-1"/>
</node>
</node>
<node COLOR="#ff00ff" CREATED="1633942943850" ID="ID_944075307" MODIFIED="1633943445615" POSITION="left" TEXT="Software bajo copyleft">
<icon BUILTIN="back"/>
<node CREATED="1633943124698" ID="ID_370291965" MODIFIED="1633943170401" TEXT="Permiten la libre distribuci&#xf3;n de copias y versiones modificadas">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1633943183971" ID="ID_554329089" MODIFIED="1633943206930" TEXT="Ejercidas por autores de c&#xf3;digo">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1633943210322" ID="ID_1054656816" MODIFIED="1633943247866" TEXT="Los derechos concedidos deben matenerse en las modificaciones">
<icon BUILTIN="full-3"/>
</node>
</node>
<node COLOR="#ff00ff" CREATED="1633942961011" ID="ID_1829260081" MODIFIED="1633943469791" POSITION="right" TEXT="Software bajo GPL">
<icon BUILTIN="forward"/>
<node CREATED="1633943259411" ID="ID_1851540971" MODIFIED="1633943273714" TEXT="Licencia P&#xfa;blica General">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1633943277642" ID="ID_1604798765" MODIFIED="1633943299241" TEXT="Creada por la Free Software Fundation">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1633943302108" ID="ID_1549154737" MODIFIED="1633943330738" TEXT="Protege la distribuci&#xf3;n, modificaci&#xf3;n y utilizaci&#xf3;n">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1633943349859" ID="ID_1567055398" MODIFIED="1633943389834" TEXT="Impide que el software pueda ser integrado en otro software privativo">
<icon BUILTIN="full-4"/>
</node>
</node>
<node COLOR="#ff00ff" CREATED="1633943518740" ID="ID_1494879875" MODIFIED="1633943734879" POSITION="left" TEXT="Software bajo licencias laxas o permisivas">
<icon BUILTIN="back"/>
<node CREATED="1633943576747" ID="ID_343972847" MODIFIED="1633943605643" TEXT="Flexible en la distribuci&#xf3;n">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1633943609603" ID="ID_1806087107" MODIFIED="1633943629874" TEXT="Puede distribuirse como libre o privativo">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1633943630722" ID="ID_1689513185" MODIFIED="1633943672339" TEXT="Sin copyleft. El derivado no tiene que matener las restricciones">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1633943673889" ID="ID_318016746" MODIFIED="1633943718554" TEXT="Ejemplos: licencias MIT">
<icon BUILTIN="full-4"/>
</node>
</node>
<node COLOR="#ff00ff" CREATED="1633943544202" ID="ID_285271787" MODIFIED="1633943832400" POSITION="right" TEXT="Software de prueba (Shareware)">
<icon BUILTIN="forward"/>
<node CREATED="1633943753499" ID="ID_1763610392" MODIFIED="1633943787018" TEXT="Autorizan al usuario a evaluar el software">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1633943791644" ID="ID_312413722" MODIFIED="1633943822891" TEXT="Software con limitaciones en tiempo o en funcionalidades">
<icon BUILTIN="full-2"/>
</node>
</node>
</node>
</map>
