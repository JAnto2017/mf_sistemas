<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1633936661423" ID="ID_130529119" MODIFIED="1633954712656">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font color="#ff00ff">SISTEMAS DE GESTI&#211;N EMPRESARIAL</font></b>
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="button_ok"/>
<node BACKGROUND_COLOR="#ffff66" CREATED="1633940506691" ID="ID_1816473398" MODIFIED="1633954728744" POSITION="right" TEXT="ERP">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-1"/>
<node CREATED="1633941722217" ID="ID_543549049" MODIFIED="1633941754905" TEXT="Sistemas de planificaci&#xf3;n de recursos empresariales"/>
<node CREATED="1633941762193" ID="ID_480129032" MODIFIED="1633941775501" TEXT="Software de gesti&#xf3;n integrada"/>
<node CREATED="1633954881033" ID="ID_1181497072" MODIFIED="1633954890141" TEXT="Es un sistema &quot;Back Office&quot;"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1633940531344" ID="ID_979279644" MODIFIED="1633954732767" POSITION="left" TEXT="CRM">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-2"/>
<node CREATED="1633941666809" ID="ID_647895915" MODIFIED="1633941716965" TEXT="Sistemas de gesti&#xf3;n de la relaci&#xf3;n con los clientes"/>
<node CREATED="1633954849152" ID="ID_751681387" MODIFIED="1633954876249" TEXT="Es un sistema &quot;Front Office&quot;"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1633940543616" ID="ID_379449179" MODIFIED="1633954735728" POSITION="right" TEXT="BI">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="full-3"/>
<node CREATED="1633941821281" ID="ID_992414558" MODIFIED="1633941841857" TEXT="Soluciones de inteligencia de negocio"/>
<node CREATED="1633941846209" ID="ID_769245353" MODIFIED="1633941864169" TEXT="Soluci&#xf3;n de inteligencia empresarial"/>
<node CREATED="1633941880489" ID="ID_474286970" MODIFIED="1633941914541" TEXT="Conjunto de herramientas para facilitar datos a dirigentes empresariales">
<node CREATED="1633955978554" ID="ID_157792383" MODIFIED="1633956058883" TEXT="">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Aplicaciones
      </li>
      <li>
        Infraestructuras
      </li>
      <li>
        Herramientas
      </li>
      <li>
        Mejoras pr&#225;cticas
      </li>
      <li>
        An&#225;lisis de la informaci&#243;n
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
